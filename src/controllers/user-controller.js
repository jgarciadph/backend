const userService = require('../services/user-service');
const BaseController = require('./base-controller')

class UserController extends BaseController {
    constructor() {
        super()
    }

    get = (req, res) => {
        userService.getById(req.id, (user, err) => {
            if(err) {
                console.log(err)
                res.status(400).json({ "msg": err})
            } else {
                user.password = '';
                res.json(user);       
            }
        });
    }
}

module.exports = new UserController();
