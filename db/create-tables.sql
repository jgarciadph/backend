CREATE TABLE `hogardocente`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `telephone` VARCHAR(12) NULL,
  `birthday` DATE NULL,
  `direction` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `photo` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);


CREATE TABLE `hogardocente`.`advert` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `owner` INT NOT NULL,
  `title` VARCHAR(45) NULL,
  `description` VARCHAR(200) NULL,
  `direction` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `pricepermonth` INT NULL,
  `meters` INT NULL,
  `room` INT NULL,
  `floor` INT NULL,
  `garage` INT NULL,
  `pricegarage` INT NULL,
  `bathrooms` INT NULL,
  `balcony` INT NULL,
  `terrace` INT NULL,
  `furnished` INT NULL,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `owner_fk_idx` (`owner` ASC) VISIBLE,
  CONSTRAINT `owner_fk`
    FOREIGN KEY (`owner`)
    REFERENCES `hogardocente`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `hogardocente`.`photo` (
  `advert` INT NOT NULL,
  `path` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`advert`, `path`),
  CONSTRAINT `advert_fk`
    FOREIGN KEY (`advert`)
    REFERENCES `hogardocente`.`advert` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);