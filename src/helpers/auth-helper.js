const jwt = require('jsonwebtoken');

class AuthHelper {
    constructor() {}

    generateAccessToken(email) {
        return jwt.sign({ data: email }, process.env.JWT_TOKEN_SECRET, { expiresIn: '1800s' });
    }
}

module.exports = new AuthHelper();