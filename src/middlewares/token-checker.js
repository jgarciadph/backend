const jwt = require('jsonwebtoken');

class TokenChecker {
    constructor() {}

    checkAuthToken = (req, res, next) => {
      const authHeader = req.headers['authorization']
      const token = authHeader && authHeader.split(' ')[1]
    
      if (token == null) return res.sendStatus(401)
    
      jwt.verify(token, process.env.JWT_TOKEN_SECRET, (err, id) => {
        if (err){
          console.log(err)
          return res.sendStatus(403)
        } 
    
        req.id = id.data
    
        next()
      })
    }
}

module.exports = new TokenChecker();
