const BaseController = require('./base-controller')
const AuthHelper = require('../helpers/auth-helper')
const PasswordHelper = require('../helpers/password-helper');
const userService = require('../services/user-service');

class LoginController extends BaseController {
    constructor() {
        super()
    }

    post = (req, res) => {
        const { email, password } = req.body;
        const hashedPassword = PasswordHelper.getHashedPassword(password);

        userService.getByEmailAndPassword(email, hashedPassword, (user, err) => {
            if(err) {
                console.log(err)
                res.status(400).json({ "apiErrorCode": err})
            } else {
                const accessToken = AuthHelper.generateAccessToken(user.id);
                res.json({ "auth-token": accessToken });       
            }
        });
    }
}

module.exports = new LoginController();
