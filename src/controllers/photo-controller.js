const AdvertModel = require('../models/advert-model');
const advertService = require('../services/advert-service');
const PhotoService = require('../services/photo-service');
const BaseController = require('./base-controller');
const path = require('path');
const ErrorCodes = require('../codes/error.codes');
const FileSystemHelper = require('../helpers/file-system-helper');

class PhotoController extends BaseController {
    constructor() {
        super()
    }

    getPhotos = (req, res) => {
        var advertId = req.params.advertId;
        if (isNaN(parseInt(advertId))) {
            res.status(400).send({ "apiErrorCode": ErrorCodes.ADVERT_ID_IS_NOT_NUMBER_CODE })
        } else {
            advertService.getPhotos(advertId, (paths, errCode) => {
                if (errCode) {
                    console.log(errCode)
                    res.status(400).send({ "apiErrorCode": errCode })
                } else {
                    res.json(paths);
                }
            });
        }
    }

    getPhotoImage = (req, res) => {
        const advertId = req.params.advertId;
        const imagePath = req.params.imagePath;
        const photoPath = path.join(FileSystemHelper.getAdvertPhotosPath(advertId), imagePath);

        if (FileSystemHelper.checkIfAdvertPhotoExists(photoPath))
            res.sendFile(photoPath);
        else
            res.status(400).send({ "apiErrorCode": ErrorCodes.ADVERT_PHOTO_NOT_EXISTS_CODE })
    }

    post = (req, res) => {
        if (req.files) {
            //falta generar un numero aleatorio como nombre de la imagen para que se pueda subir la misma imagen varias veces
                PhotoService.create(req.params.advertId, req.files, (insertId, errCode) => {
                    if (errCode) {
                        res.status(400).send({ "apiErrorCode": errCode })
                    } else {
                        res.json(req.files);
                    }
                })
        }
        else throw 'No file found in request';
    }
}

module.exports = new PhotoController();
