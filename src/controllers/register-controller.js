const BaseController = require('./base-controller')
const AuthHelper = require('../helpers/auth-helper')
const PasswordHelper = require('../helpers/password-helper');
const userService = require('../services/user-service');
const emailSenderHelper = require('../helpers/email-sender-helper');

class RegisterController extends BaseController {
    constructor() {
        super()

        this.authTokens = {};
    }

    post = (req, res) => {
        const { email, password } = req.body;
        const hashedPassword = PasswordHelper.getHashedPassword(password);

        userService.create(email, hashedPassword, (insertId, err) => {
            if(err) {
                console.log(err)
                res.status(400).json({ "msg": err})
            } else {
                emailSenderHelper.sendRegisteredMail(email);

                const accessToken = AuthHelper.generateAccessToken(insertId);
                res.json({ "auth-token": accessToken });       
            }
        });
    }
}

module.exports = new RegisterController();
