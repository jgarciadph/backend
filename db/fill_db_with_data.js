const passwordHelper = require("../src/helpers/password-helper");
const userService = require("../src/services/user-service");
const dotenv = require('dotenv');
const { getIds } = require("./get_users_id");
const advertService = require("../src/services/advert-service");
const csv = require('csv-parser');
const fs = require('fs');
const photoService = require("../src/services/photo-service");
const path = require('path');
const DBConnector = require("../src/services/db/db-connector");
const geocoderHelper = require("../src/helpers/geocoder-helper");

dotenv.config();

var fillAdvertsOneTime = false;
callbackCreateUsers = (insertId, err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("Inserted new user with id: " + insertId)

        if (!fillAdvertsOneTime) {
            fillAdverts(insertId);
            fillAdvertsOneTime = true;
        }
    }
}

callbackCreateAdverts = (insertId, err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("Inserted new advert with id: " + insertId)
    }
}

fillUsers = () => {
    console.log("Creating users....")
    userService.create(`test@gmail.com`, passwordHelper.getHashedPassword('1111'), callbackCreateUsers)
    for (let i = 0; i < 20; i++)
        userService.create(`test${i}@gmail.com`, passwordHelper.getHashedPassword('1111'), callbackCreateUsers)
}

fillAdverts = (userId) => {
    console.log("Creating adverts....")
    fs.createReadStream('../getDataFromIdealista/data1648460819.872.csv')
        .pipe(csv({ separator: ";", headers: true }))
        .on('data', (row) => {
            geocoderHelper.options.apiKey = "AIzaSyC5ufkdrFvi3PK4bekQ-Tqsrv3pZAjHQxg";
            geocoderHelper.getLatLongFromAddress(row["_3"], (location, err) => {
                advertService.create(userId, row["_1"], row["_2"], row["_3"], row["_4"], row["_5"], 400, 124, 3, 2, 1, 40, 2, 0, 0, 1, location.lat, location.lng, (advertId, err) => {
                    const directoryPath = path.join(__dirname, `../../getDataFromIdealista/images/${row["_8"]}`);
                    fs.readdir(directoryPath, function (err, files) {
                        if (err) {
                            return console.log('Unable to scan directory: ' + err);
                        }
                        files.forEach(function (file) {
                            const directoryImagesPath = path.join(__dirname, `../../backend/adverts-photos-filesystem/advert${advertId}/`);
                            const imagePath = path.join(__dirname, `../../backend/adverts-photos-filesystem/advert${advertId}/${file}`);
                            photoService.create(advertId, `/adverts/${advertId}/photos/${file}`, (id, err) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    fs.mkdir(directoryImagesPath, (err) => {
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            fs.copyFile(`${directoryPath}/${file}`, imagePath, (err) => {
                                                if (err) throw err;
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    });
                })
            })
            advertService.create(userId, row["_1"], row["_2"], row["_3"], row["_4"], row["_5"], 400, 124, 3, 2, 1, 40, 2, 0, 0, 1, (advertId, err) => {
                const directoryPath = path.join(__dirname, `../../getDataFromIdealista/images/${row["_8"]}`);
                fs.readdir(directoryPath, function (err, files) {
                    if (err) {
                        return console.log('Unable to scan directory: ' + err);
                    }
                    files.forEach(function (file) {
                        const directoryImagesPath = path.join(__dirname, `../../backend/adverts-photos-filesystem/advert${advertId}/`);
                        const imagePath = path.join(__dirname, `../../backend/adverts-photos-filesystem/advert${advertId}/${file}`);
                        photoService.create(advertId, `/adverts/${advertId}/photos/${file}`, (id, err) => {
                            if (err) {
                                console.log(err);
                            } else {
                                fs.mkdir(directoryImagesPath, (err) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        fs.copyFile(`${directoryPath}/${file}`, imagePath, (err) => {
                                            if (err) throw err;
                                        });
                                    }
                                });
                            }
                        });
                    });
                });
            })
        })
        .on('end', () => {
            console.log('CSV file successfully processed');
        });
}

deleteAllUsers = () => {
    console.log("Deleting users...")
    DBConnector.createConnection()
        .query(`DELETE FROM user`, function (err, results, fields) {
            if (err) {
                console.log(err)
            }
            fillUsers();
        });
}

initFill = () => {
    deleteAllUsers();
}


initFill();
