const express = require('express');
const morgan = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors')
const swaggerUi = require('swagger-ui-express');
const path = require('path')

dotenv.config();
const app = express();

app.set('port', process.env.PORT || 3000);
app.set('json spaces', 2)

app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

app.use(cors())

dotenv.config();
app.use(express.static(path.join(__dirname, '/../dist/frontend')))

swaggerDocument = require('../openapi.json');
app.use('/api/docs', swaggerUi.serve,  swaggerUi.setup(swaggerDocument));

app.use('/', require('./routes/routes'));

///////////
// FRONT PAGES
///////////
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/../dist/frontend/index.html'))
})

///////////
// START CODE
//////////
app.listen(app.get('port'),()=>{
    console.log(`Server listening on port ${app.get('port')}`);
});