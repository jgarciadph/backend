const BaseService = require("./base-service")
const DBConnector = require("./db/db-connector")

class PhotoService extends BaseService {
    constructor() {
        super();
    }

    create = (advertId, files, callback) => {
        var valuesQuery = "VALUES"
        for (var i = 0; i < files.length; i++) {
            if (i == files.length - 1)
                valuesQuery = valuesQuery.concat(`(${advertId},"${files[i].filename}")`)
            else
                valuesQuery = valuesQuery.concat(`(${advertId},"${files[i].filename}"),`)
        }
        DBConnector.createConnection()
            .query("INSERT INTO photo(advert, path) " + valuesQuery, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, `Some error was launched with: advertId=${advertId}`)
                } else {
                    callback(results.insertId, undefined);
                }
            });
    }

    update = () => { }

    delete = () => { }
}

module.exports = new PhotoService();