const ErrorCodes = require("../codes/error.codes");
const UserModel = require("../models/user-model");
const BaseService = require("./base-service")
const DBConnector = require("./db/db-connector")

class UserService extends BaseService {
    constructor() {
        super();
    }

    getByEmailAndPassword = (email, hashedPassword, callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM user WHERE email="${email}" AND password="${hashedPassword}"`, function (err, results, fields) {
                if(err) {
                    console.log(err)
                }
                if(results.length > 0) {
                    var user = new UserModel();
                    user.setData(results[0]);
                    callback(user, undefined);
                } else {
                    callback(undefined, ErrorCodes.LOGIN_ERROR_CODE);
                }
            });
    }

    getById = (id, callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM user WHERE id=${id}`, function (err, results, fields) {
                if(err) {
                    console.log(err)
                }
                if(results.length > 0) {
                    var user = new UserModel();
                    user.setData(results[0]);
                    callback(user, undefined);
                } else {
                    callback(undefined, ErrorCodes.USER_NOT_FOUND_CODE)
                }
            });
    }

    create = (email, hashedPassword, callback) => {
        DBConnector.createConnection()
            .query(`INSERT INTO user(email, password, created_at) VALUES("${email}","${hashedPassword}", now())`, function (err, results, fields) {
                if(err) {
                    if(err.code == "ER_DUP_ENTRY") {
                        callback(undefined, ErrorCodes.REGISTER_DUPLICATED_EMAIL_CODE)
                    } else {
                        console.log(err)
                        callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                    }
                } else {
                    if(results.insertId) {
                        callback(results.insertId, undefined);
                    } else {
                        callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                    }
                }
            });
    }

    update = () => {}

    delete = (id, callback) => {
        DBConnector.createConnection()
        .query(`DELETE FROM user WHERE id=${id}`, function (err, results, fields) {
            if(err) {
                console.log(err)
            }
            if(results.affectedRows == 1) {
                callback(1, undefined);
            } else if(results.affectedRows == 0) {
                callback(undefined, ErrorCodes.USER_NOT_FOUND_CODE)
            } else {
                callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
            }
        });
    }
}

module.exports = new UserService();