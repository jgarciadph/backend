module.exports = class AdvertModel {
    constructor() {
        this.id = -1;
        this.owner = -1;
        this.title = '';
        this.description = '';
        this.direction = '';
        this.city = '';
        this.country = '';
        this.pricepermonth = -1;
        this.meters = -1;
        this.rooms = -1;
        this.floor = -1;
        this.pricegarage = -1;
        this.bathrooms = -1;
        this.garage = false;
        this.balcony = false;
        this.terrace = false;
        this.furnished = false;
        this.created_at = false;
        this.updated_at = false;
        this.lat = -1;
        this.lng = -1;
        this.distance = 0;
    }

    setData = (data) => {
        this.id = data.id;
        this.owner = data.owner;
        this.title = data.title;
        this.description = data.description;
        this.direction = data.direction.replaceAll(", undefined", "");
        this.city = data.city;
        this.country = data.country;
        this.pricepermonth = data.pricepermonth;
        this.meters = data.meters;
        this.rooms = data.rooms;
        this.floor = data.floor;
        this.pricegarage = data.pricegarage;
        this.bathrooms = data.bathrooms;
        this.garage = data.garage;
        this.balcony = data.balcony;
        this.terrace = data.terrace;
        this.furnished = data.furnished;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;
        this.lat = data.lat;
        this.lng = data.lng;
        this.distance = data.distance;
    }

    static getMandatoryFields = () => {
        return [ "title","description","direction","city","country","pricepermonth","meters","rooms","floor","bathrooms" ]
    }
}