const { Router } = require('express');
const LoginController = require('../controllers/login-controller');
const RegisterController = require('../controllers/register-controller');
const AdvertController = require('../controllers/advert-controller');
const TokenChecker = require('../middlewares/token-checker');
const UserController = require('../controllers/user-controller');
const PhotoController = require('../controllers/photo-controller');
const Multer = require('multer');
const FileSystemHelper = require('../helpers/file-system-helper');

const router = Router();
var upload = Multer({ storage: Multer.diskStorage( { destination: FileSystemHelper.getStorageDestination, filename: FileSystemHelper.getStorageFilename }) });

//Auth
router.post('/api/login', LoginController.post);
router.post('/api/register', RegisterController.post);
router.get('/api/user', TokenChecker.checkAuthToken, UserController.get);

//Adverts
router.get('/api/adverts', TokenChecker.checkAuthToken, AdvertController.getAllFromUser);
router.get('/api/adverts/latest', AdvertController.getLatest);
router.get('/api/adverts/recommended', TokenChecker.checkAuthToken, AdvertController.getRecommended);
router.post('/api/adverts', TokenChecker.checkAuthToken, AdvertController.post);
router.get('/api/adverts/search', AdvertController.getBySearch)
router.get('/api/adverts/:advertId', AdvertController.getById);
router.delete('/api/adverts/:advertId', AdvertController.delete);

//AdvertId
router.get('/api/adverts/:advertId/photos', PhotoController.getPhotos);
router.post('/api/adverts/:advertId/photos', upload.array('photos'), PhotoController.post);
router.get('/api/adverts/:advertId/photos/:imagePath', PhotoController.getPhotoImage);



module.exports = router;