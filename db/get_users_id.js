const DBConnector = require("../src/services/db/db-connector");

getIds = (callback) => {
    DBConnector.createConnection()
        .query(`SELECT * FROM user`, function (err, results, fields) {
            if (err) {
                console.log(err)
            }
            callback(results);
        });
}

module.exports = { getIds };