const ErrorCodes = require("../codes/error.codes");
const AdvertModel = require("../models/advert-model");
const BaseService = require("./base-service")
const DBConnector = require("./db/db-connector")

class AdvertService extends BaseService {
    constructor() {
        super();
    }

    getAllFromUser = (id, callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM advert WHERE owner="${id}"`, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    var adverts = []
                    if (results != undefined) {
                        for (let i = 0; i < results.length; i++) {
                            var advert = new AdvertModel();
                            advert.setData(results[i]);
                            adverts.push(advert)
                        }
                    }
                    callback(adverts, undefined)
                }
            });
    }

    getById = (id, callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM advert WHERE id="${id}"`, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    if (results.length != 0) {
                        var advert = new AdvertModel();
                        advert.setData(results[0]);
                        callback(advert, undefined)
                    } else {
                        callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                    }

                }
            });
    }

    getLatest = (callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM advert ORDER BY created_at DESC LIMIT 4`, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    var adverts = []
                    if (results != undefined) {
                        for (let i = 0; i < results.length; i++) {
                            var advert = new AdvertModel();
                            advert.setData(results[i]);
                            adverts.push(advert)
                        }
                    }
                    callback(adverts, undefined)
                }
            });
    }

    getPhotos = (advertId, callback) => {
        DBConnector.createConnection()
            .query(`SELECT * FROM photo WHERE advert=${advertId}`, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    if (results.length > 0) {
                        var paths = []
                        for (let i = 0; i < results.length; i++)
                            paths.push(results[i].path)

                        callback(paths, undefined)
                    } else {// Las fotos van a ser obligatorias al menos 1, por lo que se considera error
                        callback(undefined, ErrorCodes.ADVERT_NOT_EXISTS_OR_NOT_PHOTOS_CODE)
                    }
                }
            });
    }

    getBySearch = (lat, lng, callback) => {
        DBConnector.createConnection()
            .query(`SELECT *, SQRT( POW(69.1 * (lat - ${lat}), 2) + POW(69.1 * (${lng} - lng) * COS(lat / 57.3), 2)) AS distance FROM advert HAVING distance < 500 ORDER BY distance;`, function (err, results, fields) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    if (results.length > 0) {
                        var adverts = []
                        if (results != undefined) {
                            for (let i = 0; i < results.length; i++) {
                                var advert = new AdvertModel();
                                advert.setData(results[i]);
                                adverts.push(advert)
                            }
                        }

                        callback(adverts, undefined)
                    } else {// Las fotos van a ser obligatorias al menos 1, por lo que se considera error
                        callback(undefined, ErrorCodes.ADVERT_NOT_EXISTS_OR_NOT_PHOTOS_CODE)
                    }
                }
            });
    }

    getRecommended = (email, callback) => {
        callback(undefined, "not implement")// Created a recommendation system
    }

    create = (owner, data, callback) => {
        const fullQuery = this._getCreateQueryFromData(owner, data);
        DBConnector.createConnection()
            .query(fullQuery, function (err, results, _) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    if (results.insertId) {
                        callback(results.insertId, undefined);
                    } else {
                        callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                    }
                }
            });
    }

    _getCreateQueryFromData = (owner, data) => {
        var insertIntoQuery = 'INSERT INTO advert(owner,'
        var valuesQuery = `VALUES(${owner},`;
        Object.keys(data).forEach((key) => {
            insertIntoQuery = insertIntoQuery.concat(`${key},`);
            if (typeof data[key] == "string") {
                valuesQuery = valuesQuery.concat(`"${data[key]}",`);
            } else if (typeof data[key] == "number") {
                valuesQuery = valuesQuery.concat(`${data[key]},`);
            }
        })
        insertIntoQuery = insertIntoQuery.concat('created_at,updated_at)');
        valuesQuery = valuesQuery.concat('now(),now())');

        return insertIntoQuery + valuesQuery;
    }

    update = () => { }

    delete = (id, callback) => {
        const fullQuery = "DELETE FROM advert WHERE id=" + id;
        DBConnector.createConnection()
            .query(fullQuery, function (err, results, _) {
                if (err) {
                    console.log(err)
                    callback(undefined, ErrorCodes.UNKNOWN_ERROR_CODE)
                } else {
                    callback(results, undefined)
                }
            });
    }

}

module.exports = new AdvertService();