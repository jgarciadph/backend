
class EmailSenderHelper {
    constructor() { }

    static getReadMailConfig() {
        return {
            imap: {
                user: 'info@hogardocente.es',
                password: '@Medellin12',
                host: 'imap.ionos.es',
                port: 993,
                authTimeout: 10000,
                tls: true,
                tlsOptions: { rejectUnauthorized: false },
            }
        }
    }

    static getSendMailConfig() {
        return {
            host: 'smtp.ionos.es',
            port: 587,
            auth: {
                user: 'info@hogardocente.es',
                pass: '@Medellin12',
            },
        }
    }



    sendRegisteredMail(email) {
        const nodemailer = require('nodemailer');
        const transporter = nodemailer.createTransport(EmailSenderHelper.getSendMailConfig());
        transporter.sendMail({
            from: 'info@hogardocente.es',
            to: 'jgarciadg@alumnos.unex.es',//TODO: Change by email
            subject: 'Registro HogarDocente',
            html: EmailSenderHelper.getRegisterEmailBody(),
        }, (err, info) => {
            console.log(err)
            console.log(info)
        })
    }

    static getRegisterEmailBody(email) {
        return `
            <html>
                <body style="text-align: center;background-color: #f6f6f6; height:300px;font-family: sans-serif; font-size: 14px;">
                <div style="text-align:start;background-color: white;padding: 20px; margin: 20px;">
                    <p>Buenos días,</p>
                    <p>Acabamos de registrar un usuario con esta dirección de correo. Si no has sido tú, simplemente elimina este mensaje.</p>
                    <a href="https://hogardocente.herokuapp.com/" style="background-color: #ffffff; border: solid 1px; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize;background: linear-gradient(to left, #FCB031, #DD064C); color: #ffffff;">Hogar Docente</a>
                    <p>Puedes pulsar en el botón de arriba para acceder a la página y ver todos los anuncios creados.</p>
                    <p>Buena suerte encontrando tu hogar para el curso escolar.</p>
                </div>
                <p>HogarDocente. Todos los derechos reservados.</p>
                </body>
            </html>
            `;
    }
}

module.exports = new EmailSenderHelper();