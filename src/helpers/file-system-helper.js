const { dirname, join } = require('path');
const path = require('path')

const fs = require('fs');
const codeGeneratorHelper = require('./code-generator-helper');

class FileSystemHelper {
    static INDEX_FILE_DIR = dirname(require.main.filename);
    static APP_DIR = join(FileSystemHelper.INDEX_FILE_DIR, '..');
    static ADVERTS_PHOTOS_FILESYSTEM_PATH = join(FileSystemHelper.APP_DIR, 'adverts-photos-filesystem');

    constructor() { }

    static createDirectoryIfNotExists(directory) {
        if (!fs.existsSync(directory)) {
            fs.mkdir(directory, (err) => {
                if (err) {
                    return console.error(err);
                }
                console.log('Directory created successfully!');
            });
        }
    }

    static checkIfAdvertPhotoExists(photoPath) {
        try {
            if (fs.existsSync(photoPath)) {
                return true;
            }
        } catch (err) {
            return false;
        }
    }

    static getStorageDestination(req, file, cb) {
        const advertId = req.params.advertId;
        const advertPhotosPath = FileSystemHelper.getAdvertPhotosPath(advertId);
        FileSystemHelper.createDirectoryIfNotExists(advertPhotosPath);

        cb(null, advertPhotosPath)
    }

    static getStorageFilename(req, file, cb) {
        console.log(file)
        cb(null, codeGeneratorHelper.getRandomCode() + path.extname(file.originalname));
    }

    static getAdvertPhotosPath(advertId) {
        return join(FileSystemHelper.ADVERTS_PHOTOS_FILESYSTEM_PATH, "/advert" + advertId)
    }
}

module.exports = FileSystemHelper;