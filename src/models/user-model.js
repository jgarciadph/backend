module.exports = class UserModel {
    constructor() {
        this.id = -1;
        this.email = '';
        this.password = '';
        this.name = '';
        this.lastname = '';
        this.telephone = '';
        this.created_at = '';
        this.birthday = '';
        this.direction = '';
        this.city = '';
        this.country = '';
        this.photo = '';
    }

    setData = (data) => {
        this.id = data.id;
        this.email = data.email;
        this.password = data.password;
        this.name = data.name;
        this.lastname = data.lastname;
        this.telephone = data.telephone;
        this.created_at = data.created_at;
        this.birthday = data.birthday;
        this.direction = data.direction;
        this.city = data.city;
        this.country = data.country;
        this.photo = data.photo;
    }

}