const NodeGeocoder = require('node-geocoder');
const ErrorCodes = require('../codes/error.codes');

class GeocoderHelper {
    DEFAULT_DISTANCE_IN_SEARCH = 50;
    options = {
        provider: 'google',
        apiKey: process.env.GOOGLE_API_KEY,
        formatter: null
    };
    geocoder = NodeGeocoder(this.options);

    constructor() {}

    getLatLongFromAddress(address, callback) {
        this.geocoder.geocode(address).then((result) => {
            if(result.length != 0) {
                callback({lat: result[0].latitude, lng: result[0].longitude}, undefined)
            } else {
                callback(undefined, ErrorCodes.DIRECTION_NOT_FOUND_CODE)
            }
        })
    }
}

module.exports = new GeocoderHelper();