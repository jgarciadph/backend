const ErrorCodes = require('../codes/error.codes');
const GeocoderHelper = require('../helpers/geocoder-helper');
const AdvertModel = require('../models/advert-model');
const AdvertService = require('../services/advert-service');
const BaseController = require('./base-controller')

class AdvertController extends BaseController {
    constructor() {
        super()
    }

    getAllFromUser = (req, res) => {
        const id = req.id;

        AdvertService.getAllFromUser(id, (adverts, errorCode) => {
            if (errorCode) {
                console.log(errorCode)
                res.status(400).json({ "apiErrorCode": errorCode })
            } else {
                res.json(adverts);
            }
        })
    }

    getById = (req, res) => {
        var advertId = req.params.advertId;

        AdvertService.getById(advertId, (advert, errorCode) => {
            if (errorCode) {
                console.log(errorCode)
                res.status(400).json({ "apiErrorCode": errorCode })
            } else {
                res.json(advert);
            }
        })
    }

    getRecommended = (req, res) => {
        res.json({ "Title": "Hola mundo desde AdvertController.getRecommended!" });
    }

    getBySearch = (req, res) => {
        var address = req.query.address;
        GeocoderHelper.getLatLongFromAddress(address, (location, err) => {
            if(err)
                console.log(err)

            if(location != undefined) {
                AdvertService.getBySearch(location.lat, location.lng, (adverts, errorCode) => {
                    if (errorCode) {
                        console.log(errorCode)
                        res.status(400).json({ "apiErrorCode": errorCode })
                    } else {
                        res.json(adverts);
                    }
                })
            } else {
                res.status(400).json({ "msg": "no location found" })
            }
        })
    }

    getLatest = (req, res) => {
        AdvertService.getLatest((adverts, err) => {
            if (err) {
                console.log(err)
                res.status(400).json({ "msg": err })
            } else {
                res.json(adverts);
            }
        })
    }

    post = (req, res) => {
        const data = req.body;

        var errors = [];
        this._validateAdvertData(data, errors);
        if (errors.length != 0) {
            res.status(400).json({ "errors": errors })
        }

        if(data.city)
            data.direction = data.direction + ', ' + data.city;
        this._validateDirection(data.direction, (location, error) => {
            if (error == undefined) {
                var userId = req.id;
                data.lat = location.lat;
                data.lng = location.lng;
                AdvertService.create(userId, data, (insertId, err) => {
                    if (err) {
                        console.log(err)
                        res.status(400).json({ "msg": err })
                    } else {
                        res.json(insertId);
                    }
                })
            } else {
                errors.push(error)
                res.status(400).json({ "errors": errors })
            }
        });
    }

    delete  = (req, res) => {
        const id = req.params.advertId;

        AdvertService.delete(id, (_, errorCode) => {
            if (errorCode) {
                console.log(errorCode)
                res.status(400).json({ "apiErrorCode": errorCode })
            } else {
                res.json({"apiErrorCode": undefined});
            }
        })
    }

    _validateAdvertData = (data, errors) => {
        Object.keys(data).forEach((key) => {
            const value = data[key];
            if (typeof value == "string")
                this._validateString(value, key, errors);
            else if (typeof value == "number")
                this._validateInt(value, key, errors);
        })

        this._validateCreatedAndUpdatedAt(data, errors);
    }

    _validateString = (string, nameField, errors) => {
        if ((string == undefined || string == "" || string.replaceAll(' ', '') == "") && AdvertModel.getMandatoryFields().includes(nameField)) {
            switch (nameField) {
                case "title":
                    errors.push(ErrorCodes.TITLE_EMPTY_CODE);
                    break;
                case "description":
                    errors.push(ErrorCodes.DESCRIPTION_EMPTY_CODE);
                    break;
                case "direction":
                    errors.push(ErrorCodes.DIRECTION_EMPTY_CODE);
                    break;
                case "city":
                    errors.push(ErrorCodes.CITY_EMPTY_CODE);
                    break;
                case "country":
                    errors.push(ErrorCodes.COUNTRY_EMPTY_CODE);
                    break;
            }
        }
    }

    _validateInt = (number, nameField, errors) => {
        if ((number == undefined || number == -1) && AdvertModel.getMandatoryFields().includes(nameField)) {
            switch (nameField) {
                case "pricepermonth":
                    errors.push(ErrorCodes.PRICE_PER_MONTH_NOT_VALID);
                    break;
                case "meters":
                    errors.push(ErrorCodes.METTERS_NOT_VALID);
                    break;
                case "rooms":
                    errors.push(ErrorCodes.ROOMS_NOT_VALID);
                    break;
                case "floor":
                    errors.push(ErrorCodes.FLOOR_NOT_VALID);
                    break;
                case "pricegarage":
                    errors.push(ErrorCodes.PRICE_GARAGE_NOT_VALID);
                    break;
                case "bathrooms":
                    errors.push(ErrorCodes.bathrooms);
                    break;
            }
        }
    }

    _validateDirection = (direction, callback) => {
        GeocoderHelper.getLatLongFromAddress(direction, (result, err) => {
            if (err) {
                callback(undefined, err)
            } else {
                callback(result, undefined)
            }
        });
    }

    _validateCreatedAndUpdatedAt = (data, errors) => {
        Object.keys(data).forEach((key) => {
            if(key == "created_at") {
                errors.push(ErrorCodes.CREATED_AT_MUST_NOT_TO_BE_CODE);
            }
            if(key == "updated_at"){
                errors.push(ErrorCodes.UPDATED_AT_MUST_NOT_TO_BE_CODE);
            }
        })
    }

}

module.exports = new AdvertController();
