const mysql = require('mysql');

module.exports = class DBConnector {
  static connection = undefined;

  constructor() {}

  static createConnection() {
    if(this.connection == undefined) {
      DBConnector.handleDisconnect();
    } 
    return this.connection;
  }

  static checkConnection() {
    this.createConnection().connect(function(err) {
      if(err) throw err;
      console.log('Connected to the MySQL server.');
    });
  }

  static handleDisconnect() {
    this.connection = mysql.createConnection({
      host: process.env.MYSQL_HOST,
      port: process.env.MYSQL_PORT,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASS,
      database: process.env.MYSQL_DATABASE
    });

    this.connection.connect(function(err) {         
      if(err) {
        console.log('error when connecting to db:', err);
        this.connection = undefined;
        setTimeout(DBConnector.handleDisconnect, 2000);
      }
    });
                                            
    this.connection.on('error', function(err) {
      console.log('Restarting db connection...');
      this.connection = undefined;
      if(err.code === 'PROTOCOL_CONNECTION_LOST') {
        DBConnector.handleDisconnect();
      } else {
        throw err;
      }
    });
  }
}